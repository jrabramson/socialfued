import React from 'react';
import { TweenLite, Back } from'gsap';
import Sound from 'react-sound';
import { compose, lifecycle, withStateHandlers, withHandlers }  from 'recompose';
import axios from 'axios';

import Ding from './answer.mp3';
import StrikeNoise from './strike.mp3';
import logo from './logo.png'
import strike from './strike.png'
import './App.css';
import data from './game.json';

const App = compose(
	withStateHandlers(
		({
			questionIndex = 0,
			questions = data,
      playStatus =  Sound.status.STOPPED,
      flips = [false, false, false, false, false],
      cards = [],
      count = 0,
      houseResponses = [],
      wrong = false
    }) => ({
		  questionIndex,
			questions,
      playStatus,
      flips,
      cards,
      count,
      houseResponses,
      wrong
		}),
		{
		  nextQuestion: ({ questions, questionIndex,cards }) => () => {
		  	TweenLite.set(cards, { rotationX: 0 });
		  	return {
			    questionIndex:  Math.min(
	          questionIndex + 1,
	          questions.length - 1
	        ),
	        playStatus: Sound.status.STOPPED,
	        flips: [false, false, false, false, false]
	      }
		  },
		  prevQuestion: ({ questionIndex, cards }) => () => {
		  	TweenLite.set(cards, { rotationX: 0 });
		    return { questionIndex: Math.max(
         	 questionIndex - 1,
	          0
	        ),
	        playStatus: Sound.status.STOPPED,
	        flips: [false, false, false, false, false]
	      }
		  },
		  playSound: ({ playStatus, count }) => () => ({
		    playStatus: Sound.status.PLAYING,
		    count: (count + 1)
		  }),
		  stopSound: ({ playStatus }) => () => ({
		    playStatus: Sound.status.STOPPED
		  }),
		  setFlip: ({ flips }) => i => {
		  	let newFlips = flips;
		  	newFlips[i] = !flips[i]
		  	return { flips: newFlips }
		  },
      setHouseResponses: ({ houseResponses }) => resp => ({
        houseResponses: resp
      }),
      setWrong: ({ wrong }) => val => ({
        wrong: val
      }),
      stopStrikeSound: ({ wrong }) => () => {
        wrong: false
      }
		}
	),
	withHandlers({
		changeQuestion: ({ questions, questionIndex, cards, prevQuestion, nextQuestion }) => event => {
			if (event.keyCode === 37) {
	      prevQuestion();
			} else if (event.keyCode === 39) {
				nextQuestion();
			}
		},
		flipCard: ({ flips, setFlip, playSound, cards }) => i => {
			setFlip(i);
			if (flips[i]) {
				TweenLite.to(cards[i], 1, { rotationX: 0, ease: Back.easeOut });
			} else {
				playSound();
				TweenLite.to(cards[i], 1, { rotationX: 180, ease: Back.easeOut });
			}
		},
    httpResponses: ({ setHouseResponses }) => () => {
	    axios.get('https://cy.rs/get_buzzers')
	      .then(function (response) {
	        setHouseResponses(response.data.houses);
	      })
	      .catch(function (error) {
	        console.log(error);
	      });
  },
	resetResponses: () => event => {
		if (event.keyCode === 82) {
			axios.get('https://cy.rs/clear_buzzers');
		}
	},
  toggleWrong: ({ setWrong }) => event => {
    if (event.keyCode === 87) {
      setWrong(true);
      setTimeout(() => setWrong(false), 1000);
    }
  }
	}),
	withHandlers({
		getOptions: ({ questions, questionIndex, flipCard, cards }) => () => questions[questionIndex].options.map(
			(option, i) => {
				return (
					<div className="cardHolder" key={i} onClick={() => flipCard(i)}>
						<div className="card" ref={(c) => cards.push(c)}>
							<div className="front">
								<span className="DBG">{i + 1}</span>
							</div>
							<div className="back DBG ">
								<span>{option.response}</span>
                <b className="LBG">{5 - i}</b>
							</div>
						</div>
					</div>
				);
			}
		),
    getResponses: ({ houseResponses }) => () => houseResponses.map((resp, i) => <div key={i}>
        {Object.keys(resp)[0]}
      </div>
    ),
		nextQuestionWithReset: ({ httpResponses, nextQuestion }) => () => {
			 axios.get('https://cy.rs/clear_buzzers')
			 	.then(function (response) {
					httpResponses();
			 		nextQuestion();
				});
		}
	}),
	lifecycle({
		componentDidMount() {
			document.addEventListener('keydown', this.props.changeQuestion, false);
			document.addEventListener('keydown', this.props.resetResponses, false);
      document.addEventListener('keydown', this.props.toggleWrong, false);
      this.timer = setInterval(()=> this.props.httpResponses(), 1000);
		},
		componentWillUnmount() {
			document.removeEventListener('keydown', this.props.changeQuestion, false);
			document.removeEventListener('keydown', this.props.resetResponses, false);
		}
	}),
);

const Container = ({ questions, questionIndex, playStatus, getOptions, nextQuestionWithReset, getResponses, stopSound, wrong, stopStrikeSound }) => <div className="gameBoard">
  <img src={logo} className="logo" alt="logo"></img>
  <div className="responses DBG">
    {getResponses()}
  </div>
	<div className={"wrong " + (wrong ? "show" : "")}>
		<img src={strike} alt="wrong" />
	</div>
	<div className="questionHolder">
		<span className="question">
			{questions[questionIndex].title}
		</span>
	</div>
	<div className="colHolder">
		<div className="col1">{getOptions()}</div>
		<div className="col2" />
	</div>
	<div className="btnHolder">
		<div onClick={nextQuestionWithReset} className="button">
			Next Question
		</div>
	</div>
  <Sound
    url={Ding}
    playStatus={playStatus}
		onFinishedPlaying={() => stopSound()}/>
  <Sound
    url={StrikeNoise}
    playStatus={wrong ? Sound.status.PLAYING : Sound.status.STOPPED}
		onFinishedPlaying={() => stopStrikeSound()}/>
</div>;

export default App(Container);
